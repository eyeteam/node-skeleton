var config = require('../../lib/config');

var r = require('rethinkdb');
var assert = require('assert');
// var JaySchema = require('jayschema');
// var js = new JaySchema();

var pool = require('../../lib/pool');


function readMetaFactory(socket, client){
	return async function (data) {
		console.log('readMetaFactory -> ', data);
		var q = data.query;
		var reqId = data.request_id;
		let metaResult = await readMeta(q.id);
		
		console.log('metaResult:',metaResult);
		//response to client via socket
		socket.emit('response', {
			request_id: reqId,
			status: metaResult ? 200 : 404,
			body: metaResult
		});
	}
}

function createMetaFactory(socket, client){
	return async function (data) {
		console.log('createMetaFactory -> ', data);
		var q = data.query;
		var b = data.body;
		var reqId = data.request_id;

		let metaResult = await saveMeta(q.id, b);
		console.log('metaResult:',metaResult);

		//response to client via socket
		socket.emit('response', {
			request_id: reqId,
			status: metaResult ? 200 : 404,
			body: metaResult
		});
	}
}



async function saveMeta(id, data){
	let connection = await pool.singleConn();
	let metaResult = await r.table('meta')
	.insert({
		id: id,
		data: data
	}, {conflict: "update"})
	.run(connection);
	return metaResult;
}

async function readMeta(id) {
	let connection = await pool.singleConn();
	let metaResult = await r.table('meta').get(id)
	.run(connection);
	if (!metaResult) {
		return false;
	}
	return metaResult.data;
}

module.exports = {
	readMetaFactory,
	createMetaFactory,
	readMeta,
	saveMeta
}
