var config = require('../lib/config');
var connect = require('../lib/connect');
var uuid = require('node-uuid');

var express = require('express');
var router = express.Router();
var r = require('rethinkdb');
var fetch = require('node-fetch');
var request = require('../lib/request')();
var meta = require('./server/meta');
var fs = require('fs');
var path = require('path');

function getId(obj) {
	if (typeof obj === "string") {
		return obj;
	}
	return obj.id;
}

function convertDate(object, attrs = ['created_time', 'updated_time']) {
	if (Array.isArray(object)) {
		for (let doc of object) {
			convertDate(doc);
		}
		return object;
	}

	for (let key of Object.keys(object)) {
		let oldVal = object[key];

		if (attrs.indexOf(key) >= 0) {
			let newVal = new Date(oldVal);
			if (isNaN(newVal)) {
				newVal = new Date(0);
			}

			object[key] = newVal;

		} else if (typeof oldVal === 'object' && oldVal !== null) {
			convertDate(oldVal);
		}
	}

	return object;
}

async function readMeta(id) {
	let connection = await connect();
	let metaResult = await r.table('meta').get(id)
	.run(connection);
	if (!metaResult) {
		return false;
	}
	return metaResult.data;
}

async function saveImage(base64){
	try {
		var filename = uuid.v4() + '.png';
		var uploadUrl = config.server.uploadUrl;
		
		fs.writeFileSync(
			path.join(__dirname, '..', 'public', 'uploads', filename),
			removeBase64Prefix(base64),
			'base64'
		);
		return uploadUrl + '/' + filename;
	} catch (e) {
		console.error(e.stack);
	}		
}


module.exports = {
	convertDate,
	readMeta,
	saveImage
}
