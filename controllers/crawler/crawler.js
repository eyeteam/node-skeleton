var config = require('../../lib/config');
var cHelper = require('./crawler-helper');
var epsTime = 100; // 1/10 second
var MSINDATE = 86400000;
var connect = require('../../lib/connect');
var r = require('rethinkdb');
var dbName = config.rethinkdb.db;

// Update status = waiting
var isCrawling = false;

// Update lastCrawlTime in first run or after restart
var lastCrawlTime = config.crawler.last_crawl_time || new Date();
lastCrawlTime = new Date(lastCrawlTime);

var _now = new Date();
var _yesterday = new Date(_now - 1 * 86400 * 1000);

var adPosts = [];
var lastCrawlAdPostTime = _yesterday;


async function main(t) {
    console.log('start crawler');
    var connection = await connect();

    if (t) {
        lastCrawlTime = t;
    }
    if (isCrawling) {
        console.log(new Date(), 'skip 1 cycle');
        return;
    }

    console.log(new Date(), ': start crawling');
    isCrawling = true;

    let _time = new Date();

    // do crawler jobs

    let timeSpend = new Date() - _time;
    console.log(new Date(), ': crawl complete - timespend = ', timeSpend);

    lastCrawlTime = _time;
    await cHelper.setCrawlTime(lastCrawlTime);

    isCrawling = false;
}

module.exports = main;