var config = require('../../lib/config');
var fetch = require('node-fetch');
var connect = require('../../lib/connect');
var r = require('rethinkdb');
var MSINDATE = 86400000;
var request = require('../../lib/request')();;

import { convertDate } from '../helper';

function eqTime(t1, t2, eps) {
	console.log('::eqTime', t1.id);
	if (t1.updated_time) {
		t1 = t1.updated_time;
	}
	if (t2.updated_time) {
		t2 = t2.updated_time;
	}
	var d1 = new Date(t1);
	var d2 = new Date(t2);
	// console.log(d2, d1);
	var gap = Math.abs(d2.getTime() - d1.getTime());
	if (isNaN(gap)) {
		gap = 0;
	}
	if (gap < eps) {
		console.log('____eqTime ->', true);
		return true;
	}
	console.log('____eqTime ->', false);
	return false;
}

async function sendCallback(body) {
	let url = config.crawler.callback_url;
	var sep = '?';
	if (url.indexOf('?') > -1) {
		sep = '&';
	}
	url += sep + 'crawler=1';

	let res = await request.post(url, body);
	if (typeof res === 'string') {
		try {
			let json = JSON.parse(res);
			return json;
		}
		catch (e) {
			// do nothing
		}
	}
	console.log('sendCallback:', body);
	console.log('result:')
	console.log(res);
	return res;
}


async function saveDocs(docs, tableName) {
	var connection = await connect();
	console.log('\n------------saveDocs------------');
	var newDocs = convertDate(docs);
	console.log('old:', docs);
	console.log('new:', newDocs);
	console.log(':::::::::::::saveDocs::::::::::::\n');
	var result = await r.table(tableName).insert(newDocs, {
		conflict: 'update'
	}).run(connection);
	return result;
}

async function getDocsByIds(ids, tableName) {
	console.log('::getDocsByIds', ids, tableName);
	if (!ids.length) {
		return [];
	}
	let connection = await connect();
	let res = await r.table(tableName).getAll(r.args(ids))
	.coerceTo('array').run(connection);
	return res;
}



async function saveMeta(id, data){
	let connection = await connect();
	let metaResult = await r.table('meta')
	.insert({
		id: id,
		data: data
	}, {conflict: "update"})
	.run(connection);
	return metaResult;
}

async function readMeta(id) {
	let connection = await connect();
	let metaResult = await r.table('meta').get(id)
	.run(connection);
	if (!metaResult) {
		return false;
	}
	return metaResult.data;
}


async function seq(func, inputs) {
	var result = [];
	return new Promise(async function(resolve, _) {
		for (let input of inputs) {
			try {
				let r = await func(input);
				result.push(r);	
			} catch (e) {
				//do nothing
			}
		}
		resolve(result);	
	});
}

async function parallel(func, inputs, n) {
	var arr = [];
	for (let i = 0; i < n; i++) {
		arr.push([]);
	}
	let i = 0;
	for (let input of inputs) {
		arr[i].push(input);
		i++;
		if (i == n) {
			i = 0;
		}
 	}
 	var promises = arr.map(v => seq(func, v));
 	var results = await Q.all(promises);
 	var merged = [].concat.apply([], results);
 	return merged;
}

async function setCrawlTime(d) {
	var res = await saveMeta('last_crawl_time', new Date(d));
	return res;
}

async function getLastCrawlTime() {
	var res = await readMeta('last_crawl_time');
	if (!res || !res.data) {
		return false;
	}
	return new Date(res.data);
}

module.exports = {
	sendCallback,
	eqTime,				// eqTime(post|string|int, post|string|int, int)
	saveDocs,			// savePost(docArr, tableName)
	getDocsByIds, 		// getDocsByIds(idArr, tableName),
	seq,
	parallel,
	setCrawlTime,
	getLastCrawlTime
};

