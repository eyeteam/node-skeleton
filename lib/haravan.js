var fetch = require('node-fetch');
var assert = require('assert');
var config = require('../lib/config');
var baseUrl = config.flash.base_url;

async function _get(url) {
	console.log(new Date(), 'GET: ', url);
	let res = await fetch(url);
	let json = await res.json();
	return json;
}


async function _put(url, body) {
	console.log(new Date(), 'PUT: ', url, body);
	let res = await fetch(url, {
		method: 'put',
		headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json'
		},
		body: JSON.stringify(body)
	});
	let json = await res.json();
	return json;
}

async function _post(url, body) {
	console.log(new Date(), 'POST: ', url, body);
	let res = await fetch(url, {
		method: 'post',
		headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json'
		},
		body: JSON.stringify(body)
	});
	let json = await res.json();
	return json;
}

async function _delete(url) {
	console.log(new Date(), 'DELETE: ', url);
	let res = await fetch(url, {
		method: 'delete',
		headers: {
		    'Accept': 'application/json',
		    'Content-Type': 'application/json'
		},
	});
	let json = await res.json();
	return json;
}


function parseUrl(api) {
	if (api[0] == '/') {
		api = api.substr(1);
	}
	var url = baseUrl + '/' + api;
	return url;
}

async function get(api) {
	let url = parseUrl(api);
	let res = await _get(url);
	return res;
}

async function post(api, body) {
	let url = parseUrl(api);
	let res = await _post(url, body);
	return res;
}

async function put(api, body) {
	let url = parseUrl(api);
	let res = await _post(url, body);
	return res;
}


async function del(api) {
	let url = parseUrl(api);
	let res = await _delete(url);
	return res;
}

	

module.exports = {
	get: get,
	post: post,
	put: put,
	delete: del
};