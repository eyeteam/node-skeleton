var config = require('./config');

var r = require('rethinkdb');
var connect = require('./connect');

module.exports = {
  // Single request: Open maximum 100 connections to handle
  singleConn: connect,
  newSubscribePool() {
    let conns = {};
    return {
      // @param connType string: There is only one connection per connType. If new connType is created, the old one will be released.
      singleConn: connect,
      async subscribeConn(connType) {
        if (!connType) {
          throw new Error('Expect connType');
        }

        if (conns[connType]) {
          conns[connType].close();
        }

        conns[connType] = await r.connect(config.rethinkdb);
        return conns[connType];
      },
      async releaseAll() {
        let connTypes = Object.getOwnPropertyNames(conns);
        console.log('Release all connections:', connTypes);
        for (let i=0; i < connTypes.length; i++) {
          let connType = connTypes[i];
          conns[connType].close();
        }
      }
    };
  }
};
