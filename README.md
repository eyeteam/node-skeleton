# EYE Solution's Skeleton

## Technology Stack
+ NodeJS
+ Express 4.x
+ Rethinkdb 2.3.x
+ Babel 6.x
+ Socket.IO 1.3.x
  
## How to run
+ `npm install` (`sudo npm install` if linux/osx)
+ `node server` to test if runable or not


## How to use

+ Modify `config.json` for server config, ex: server port
+ Mofiry `testcases/init_db.js` to add some database index
+ Run `node test init_db` to create db and dump init data
