var express = require('express');
var router = express.Router();
var assert = require('assert');
var FB = require('../lib/facebook');
var config = require('../lib/config');
var request = require('../lib/request');
var r = require('rethinkdb');
var connect = require('../lib/connect');

// Serve index.html
var config = require('../lib/config');

var fs = require('fs');
var path = require('path');

var clientConfig = config.client;
var indexHtml = fs.readFileSync(path.join(__dirname, '../', config.server.index_html)).toString();
indexHtml = indexHtml.replace('{{CONFIG}}', JSON.stringify (clientConfig));

// some router for each customer
// var posRouter = require('./server-pos-local');
// posRouter(router);

router.get('/', function(req, res, next) {
  console.log('GET /');
  res.send(indexHtml);
});

// In development, proxy '/assets/js/' to 'http://localhost:8090/assets/js'
if (process.env.NODE_ENV == 'development') {
  console.log('Server is running in development mode:');
  try {
  } catch (e) {
    console.error(e.stack);
  }   
} else {
  
}

router.post('/login', function(req, res, next) {
  res.send(indexHtml);
});


router.all('/test', function(req, res) {
  res.send({
    query: req.query,
    body: req.body,
    params: req.params,
    headers: req.headers,
    files: req.files
  });
})

module.exports = router;
