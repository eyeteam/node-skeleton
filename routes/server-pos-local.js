var meta = require('../controllers/meta/meta');
var request = require('../lib/request');
var config = require('../lib/config');
var authorization = Buffer(config.teamcrop.key + ":" + config.teamcrop.secret).toString("base64");
var tc = request({
	headers: {
		'Content-Type': 'application/json',
		'Authorization': authorization
	}
});

module.exports = function(router) {
	
	router.get("/api/tc/region", meta.region);
	router.get("/api/tc/ordercancelreason", meta.ordercancelreason);
	router.get("/api/tc/shippingmethod", meta.shippingmethod);
	router.get("/api/tc/shippingmethoddetail", meta.shippingmethoddetail);
	router.get("/api/tc/productprice", meta.productprice);
	router.put("/api/tc/order/cancel", meta.cancelorder);
	router.post("/api/tc/order", meta.createorder);
	router.get("/api/tc/order", meta.getorder);
	router.put("/api/tc/order", meta.editorder);
	router.get("/api/tc/customer", meta.getcustomer);

	router.post("/api/order/export", meta.exportExcelOrder);
	router.post("/api/customer/export", meta.exportExcelCustomer);

	router.post("/api/tc/productreceipt", function(req, res) {
		//TODO: create product receipt + decrease inventory
		res.send({
			"success": true
		});
	});

	router.post("/api/tc/cashflow", function(req, res) {
		//TODO: create invoice and adjust cashflow
		res.send({
			"success": true
		});
	});

	router.all('/api/tc/*', function(req, res, next) {
		console.log('Teamcrop: ');

		var method = req.method.toLowerCase();
		var url = req.url;
		var pos = url.indexOf('/api/tc');
		var endpoint = url.substr(pos + '/api/tc'.length);
		var url = "https://teamcrop.com/rest" + endpoint;
		var result = tc[method](url, req.body).then(function(result) {
			res.send(result);	

			console.log(JSON.stringify({
				url: url,
				path: req.path,
				body: req.body,
				query: req.query,
				response: result 
			}, null, 2));
		})
	});

};
