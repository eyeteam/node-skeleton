require('babel-core/register');
require("babel-polyfill");
var config = require('./lib/config');

// crawler: a process without server expose (does not listen port)
var crawler = require('./controllers/crawler/crawler');
var INTERVAL_SECONDS = config.crawler.sleep_second || 30;
var timespan = INTERVAL_SECONDS * 1000;
var cHelper = require('./controllers/crawler/crawler-helper');

function start() {
	cHelper.getLastCrawlTime().then(function(lastCrawlTime) {
		var NOW = new Date();
		console.log("config.crawler.last_crawl_time -> NOW:",(NOW - new Date(config.crawler.last_crawl_time)) / 3600000, 'hrs');
		console.log("lastCrawlTime -> NOW:",(NOW - new Date(lastCrawlTime)) / 3600000, 'hrs');
		
		// run first time
		crawler(lastCrawlTime);
	
		// run after 30s - repeat :: comment it if want crawl once
		setInterval(crawler, timespan);
	}, function(err) {
		console.error(err.stack);
	});
}

start();