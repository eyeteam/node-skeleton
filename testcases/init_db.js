var config = require('../lib/config');
var tableList = config.rethinkdb.tableList;
var r = require('rethinkdb');
var connect = require('../lib/connect');

var simpleIndexs = {
	user: [
		"sex" ,
		'created_time'
	]
}

var advIndexs = {
	user: {
		"sex_adv": {
			index: r.row("sex").default("male"), 
			option: {multi: true}
		}
	}
}

async function createSimpleIndex(table, index) {
	try {
		let conn = await connect(config.rethinkdb);
		var dbName = config.rethinkdb.db;
		let connection = await connect();
		let indexList = await r.db(dbName).table(table).indexList().run(connection);
		console.log('indexList', indexList);
		if (indexList.indexOf(index) < 0) {
			//create index:
			if (index.indexOf(',') < 0) {
				// single field index
				console.log("create index", index, "for table", table);

				await r.db(dbName).table(table).indexCreate(index).run(connection);
			} else {
				//compound index
				let fields = index.split(',');
				let arr = [];
				for (let i of fields) {
				  arr.push(r.row(i));
				}
				console.log("create index", index, "for table", table);
				await r.db(dbName).table(table).indexCreate(index, arr).run(connection);
			}
		} else {
			console.log("index", index, "is existed");
		}
	} catch (e) {
		console.error(e.stack);
	}
}


async function createAdvancedIndex (table, indexName, index, option) {
	try {
		let conn = await connect(config.rethinkdb);
		var dbName = config.rethinkdb.db;
		let connection = await connect();
		let indexList = await r.db(dbName).table(table).indexList().run(connection);

		if (indexList.indexOf(indexName) < 0) {
			//create index:
			var opt = (typeof option == "object" ? option : null)
			await r.db(dbName).table(table).indexCreate(indexName, index, opt).run(connection);
		} else {
			console.log("index", indexName, "is existed");
		}
	} catch (e) {
		console.error(e.stack);
	}
}

async function main() {
	try {
		console.log("start :)");
		var conf = {
			"host": config.rethinkdb.host,
			"port": config.rethinkdb.port
		};
		let conn = await connect(conf);

		try {
			await r.dbCreate(config.rethinkdb.db).run(conn);
		}
		catch(e) {
			console.log('Databse "' + config.rethinkdb.db + '" is exsit!');
		}


		let connection = await connect();
		console.log('tableList', tableList);

		for (let tb of tableList) {
			let fail = false;
			try {
				await r.tableCreate(tb).run(connection);
			}
			catch(e) {
				// console.error(e.stack);
				fail = true;
			}
			
			if (fail) {
				console.log('create table "' + tb + '" fail!');
			} else {
				console.log('create table "' + tb + '" success!');
			}
		}


		console.log("---- create simple index ----");
		for (let table in simpleIndexs) {
			if (simpleIndexs[table].length > 0) {
				for (let index of simpleIndexs[table]) {
					console.log("create: ", table, index);
					await createSimpleIndex(table, index);
				}
			}
		}
		console.log("---- create adv index ----");
		for (let table in advIndexs) {
			for (let index in advIndexs[table]) {
				if (advIndexs[table].hasOwnProperty(index)) {
					let obj = advIndexs[table][index];
					if (obj.index) {
						console.log("create: ", table, index);
						await createAdvancedIndex(table, index, obj.index, obj.option);	
					}
				}
			}
		}
		await createInitData();
		console.log("done");
		process.exit(0);
	} catch (e) {
		console.error(e.stack);
	}
}

function createInitData() {
	//TODO: create init data, ex: insert admin user
	return false;
}

module.exports = main;